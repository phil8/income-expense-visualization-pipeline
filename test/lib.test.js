import { describe } from 'riteway'

import pipelineFactory from '../index.js'

let fp = require('funprog')

describe('the pipeline', async assert => {
    let pipeline = pipelineFactory.create(fp)
    let r = await pipeline.process([
        {
            amount: 1,
            type: 'levy',
            month: 1,
            year: 2019
        }, {
            amount: 2,
            type: 'levy',
            month: 1,
            year: 2019
        },
        {
            amount: 3,
            type: 'levy',
            month: 1,
            year: 2018
        }
    ])

    console.log(r)
        assert({
            given: 'no arguments',
            should: 'return the render data',
            actual: r.length,
            expected: 2
        })
})
