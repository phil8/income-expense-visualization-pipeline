# income-expense-visualization-pipeline Library    

A library that can transforms an array of transactions groups of transactions by 
period (year-month) and type of transaction and income/expense type.

The transaction array has to have this form:
```
[
        {
            amount: 1,
            type: 'levy',
            month: 1,
            year: 2019
        }, {
            amount: 2,
            type: 'levy',
            month: 1,
            year: 2019
        },
        {
            amount: 3,
            type: 'levy',
            month: 1,
            year: 2018
        }
]
```

The resulting
```
[ {                                                         
    type: 'levy',
    month: 1,
    year: 2018,  
    bucket: '2018-1-revenue',
    expense: 1,
    expensetype: 'expense',
    period: 24217,
    color: '#33FF33',
    total: 3,
    index: 0 }, 
  {
    type: 'levy',
    month: 1,
    year: 2019,
    bucket: '2019-1-revenue',
    expense: 1,
    expensetype: 'expense',
    period: 24229,
    color: '#11FF11',
    total: 3,
    index: 12 } 
]
```


## How to use

```
npm install income-expense-visualization-pipeline
```
