const input = 'index.js'
const sourcemap = true

export default [{
    input,
    output: {
        file: 'dist/income-expense-visualization-pipeline.mjs',
        format: 'es',
        sourcemap
    }
}, {
    input,
    output: {
        file: 'dist/income-expense-visualization-pipeline.js',
        format: 'cjs',
        sourcemap
    }
},
{
    input,
    output: {
        file: 'dist/income-expense-visualization-pipeline.umd.js',
        format: 'umd',
        name: 'income-expense-visualization-pipeline',
        sourcemap
    }
}]
