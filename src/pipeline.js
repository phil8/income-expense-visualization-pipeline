// let enrich_buckets = (r) => ({ bucket: `${r.type}-${r.year}-${r.month}-${r.amount>0 ? 'revenue' : 'expense'}`})
let enrich_buckets = (r) => ({ bucket: `${r.year}-${r.month}-${r.amount > 0 ? 'revenue' : 'expense'}` })
let enrich_expense = (r) => ({ expense: r.amount < 0 ? 0 : 1 }) // revenue or expense
let enrich_expensetype = (r) => ({ expensetype: r.amount < 0 ? 'income' : 'expense' }) // revenue or expense
let enrich_period = (r) => ({ period: parseInt(r.year) * 12 + parseInt(r.month) })
let enrich_color = (r) => ({ color: getColor(r) })

function getColor (account) {
    var letters = '0123456789ABCDEF'
    var color = '#'
    let f = 7 + parseInt(account.amount % 7)

    if (account.amount < 0) {
        color += 'FF'
        color += letters[f]
        color += letters[f]
        color += letters[f]
        color += letters[f]
    } else {
        color += letters[f]
        color += letters[f]
        color += 'FF'
        color += letters[f]
        color += letters[f]
    }
    return color
}

let balances = (x) => {
    let res = x.neighbors.reduce((items, item) => {
        let bucket = items[item.bucket] ? items[item.bucket] : Object.assign({ transactions: [] }, item)
        let prev = items[item.bucket] ? items[item.bucket].total : 0
        bucket.total = prev + parseFloat(item.amount)
        bucket.transactions.push(item)
        delete bucket.amount
        items[item.bucket] = bucket
        return items
      }, {})

    return res
}

let objectvalues = function (x) {
    return Object.values(x)
}

async function useNew (accumulator, newValue) {
    return newValue
  }

let boundsMin = null
let boundsSideEffect = (x) => {
    boundsMin = boundsMin || x.period
    if (boundsMin > x.period) { boundsMin = x.period }
    return x
}

let categories = {}
let categoryIndex = 0
let categoriesSideEffect = (x) => {
    let c = categories[x.expensetype]
    if (c === undefined) {
        categories[x.expensetype] = categoryIndex
        categoryIndex++
    }

    return x
}

let selectYear = (item) => {
    return item.year === 2018
    // return true
}

let log = x => {
    console.log(x)
    return x
}

function create (fp) {
    let funprog = fp
    return {
        process: async (data) => {
            const deltaxform = fp.compose(
                fp.assign(enrich_buckets),
                fp.assign(enrich_expense),
                fp.assign(enrich_expensetype),
                fp.assign(enrich_period),
                fp.assign(enrich_color),
                // fp.mapping(log),
                fp.neighbors(data.length),
                fp.mapping(balances),
                fp.split(objectvalues),
                fp.mapping(boundsSideEffect),
                fp.mapping(categoriesSideEffect)
            )
            var result = await fp.transduceArray2(deltaxform, useNew, [], data)

            result = result.reduced

            var mappeddata = result.map((item) => {
                // let index = ((item.period - boundsMin)*categoryIndex)+categories[item.type]
                // let index = ((item.period - boundsMin)*categoryIndex)
                let index = (item.period - boundsMin)
                Object.assign(item, { index: index })
                return item
              })

              mappeddata = mappeddata.sort((a, b) => {
                return a.index - b.index
              })

            return mappeddata
        }
    }
}

export default {
    create
}
